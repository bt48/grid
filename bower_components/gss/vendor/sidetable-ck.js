/*
 * Copyright 2012 The Polymer Authors. All rights reserved.
 * Use of this source code is goverened by a BSD-style
 * license that can be found in the LICENSE file.
 */// SideTable is a weak map where possible. If WeakMap is not available the
// association is stored as an expando property. 
var SideTable;typeof WeakMap!="undefined"&&navigator.userAgent.indexOf("Firefox/")<0?SideTable=WeakMap:function(){var e=Object.defineProperty,t=Object.hasOwnProperty,n=(new Date).getTime()%1e9;SideTable=function(){this.name="__st"+(Math.random()*1e9>>>0)+(n++ +"__")};SideTable.prototype={set:function(t,n){e(t,this.name,{value:n,writable:!0})},get:function(e){return t.call(e,this.name)?e[this.name]:undefined},"delete":function(e){this.set(e,undefined)}}}();